<?php 
	
	function enqueue_theme_scripts() {
	    wp_enqueue_style( 'parent-build', get_template_directory_uri() . '/build/css/build.css' );
	    wp_enqueue_style( 'parent-login', get_template_directory_uri() . '/build/css/login.css' );
	    wp_enqueue_style( 'child-build', get_stylesheet_directory_uri() . '/build/css/build.css', array( 'parent-build' ) );

	    wp_enqueue_script( 'theme' );
	    wp_dequeue_script( 'exec' );
	    wp_enqueue_script( 'child-build-js', get_stylesheet_directory_uri() . '/build/js/build.js' );
	    wp_enqueue_script( 'child-exec-js', get_stylesheet_directory_uri() . '/build/js/exec.js' );
	}
	add_action( 'wp_enqueue_scripts', 'enqueue_theme_scripts', 100 );

	function wpse_233129_admin_menu_items() {
	    global $menu;

	    $menu[5][0] = 'Blog Posts';

	    foreach ( $menu as $key => $value ) {
	        if ( 'edit.php' == $value[2] ) {
	            $oldkey = $key;
	        }
	    }
	    
	    // change Posts menu position in the backend
	    $newkey = 83; // use whatever index gets you the position you want
	    // if this key is in use you will write over a menu item!
	    $menu[$newkey]=$menu[$oldkey];
	    unset($menu[$oldkey]);

	}

	function render_page_links($menu_class = '', $menu_item_class = '', $menu_item_link_class = ''){
		$render_string = '<ul class="' . $menu_class . '">';

		$active_pages = get_active_pages();

		foreach($active_pages as $active_page){
			$page_name =  get_field($active_page['name'].'-alt-toggle', 'option') && !empty(get_field($active_page['name'].'-alt', 'option')) ? get_field($active_page['name'].'-alt', 'option') : str_replace('-', ' ', $active_page['name']);
			$render_string .= '<li class="' . $menu_item_class .'">' . '<a href="' . site_url() . '/' . $active_page['name'] . '" class="' . $menu_item_link_class . '">' . $page_name . '</a></li>';
		}

		$render_string .= '</ul>';

		echo $render_string;
	}

	function get_current_url(){
		global $wp;
		return home_url(add_query_arg(array(),$wp->request));
	}

	add_action( 'wp_head', 'custom_color_css' );

	function custom_color_css(){
				
		if( get_field('buttons-underlines-toggle', 'option') ):
			$color = get_field('buttons-underlines', 'option');
		?>
			<style type="text/css">
				.services-services-grid-item-header{
					background-color: <?php echo $color; ?>;
				}
			</style>
		<?php
		endif;
	}
	
	add_action( '123_after_mobile_nav', 'action_123_after_mobile_nav' );

	function action_123_after_mobile_nav(){
		?>
		<div class="mobileheaderinfo">
			<div class="mobileheaderinfo-grid">
				<div class="mobileheaderinfo-grid-item">
					<div class="mobileheaderinfo-grid-item-left">
						<i class="mobileheaderinfo-grid-item-left-icon fa fa-phone"></i>
					</div>
					<div class="mobileheaderinfo-grid-item-right">
						<a href="mailto:<?php echo get_the_phone('tel') ?>"><?php echo get_the_phone(); ?></a>
					</div>
				</div>
				<div class="mobileheaderinfo-grid-item">
					<div class="mobileheaderinfo-grid-item-left">
						<i class="mobileheaderinfo-grid-item-left-icon fa fa-map-marker"></i>
					</div>
					<div class="mobileheaderinfo-grid-item-right">
						<div><?php echo get_the_address(); ?></div>
					</div>
				</div>
			</div>
		</div>
		<div class="mobileheaderquote">
			<div class="mobileheaderquote-wrapper">
				<div class="mobileheaderquote-wrapper-text"><?php the_field('header-bar-text', 'option') ?></div>
				<div class="mobileheaderquote-wrapper-button estimate-toggle"><?php the_field('quickquote-button-text', 'option'); ?> <i class="fa fa-angle-right"></i></div>
			</div>
		</div>
		<?php
	}

	add_filter( '123_social_menus_desktop_nav', 'filter_social_menus_desktop_nav', 10, 1 );

	function filter_social_menus_desktop_nav(){
		return '';
	}

	add_filter( '123_quickquote_desktop_nav', 'filter_quickquote_desktop_nav', 10, 1 );

	function filter_quickquote_desktop_nav($content){
		return '';
	}

	add_action( '123_before_desktop_nav', 'action_123_before_desktop_nav' );

	function action_123_before_desktop_nav(){
		?>
			<div class="superheader">
				<div class="superheader-grid">
					<div class="superheader-grid-item">
						<i class="superheader-grid-item-icon fa fa-map-marker"></i>
						<div class="superheader-grid-item-content"><?php echo get_the_address(); ?></div>
					</div>
					<div class="superheader-grid-item">
						<i class="superheader-grid-item-icon fa fa-phone"></i>
						<a href="<?php echo get_the_phone('tel') ?>" class="superheader-grid-item-content"><?php echo get_the_phone(); ?></a>
					</div>

					<?php if( !get_field('quickquote-disable', 'option') ): ?>
						<div class="superheader-grid-item" id="quickquoteC2A">
							<div class="superheader-grid-item-button header-topbar-link"><?php the_field('quickquote-button-text', 'option') ?> <i class="fa fa-angle-right"></i></div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		<?php
	}

	function do_update_logo_text_image(){
		$bg = get_template_directory() . '/library/img/logo-canvas.png';

		$phpimg = new PHPImage();

		$phpimg->setDimensionsFromImage($bg);
		$phpimg->setQuality(9);
		$phpimg->setFont(get_stylesheet_directory() . '/library/fonts/Montserrat-Black.ttf');

		$text_color = array(255, 255, 255);

		// set parent theme headercolor field
		if( get_field('navs-text-toggle', 'option') ){
			$text_color = hex_to_rgb(get_field('navs-text', 'option'));
		}
		// set child theme header color field
		if( get_field('add_extra_theme_colors_header-logotoggle', 'option') ){
			$text_color = hex_to_rgb(get_field('add_extra_theme_colors_header-logopicker', 'option'));
		}


		$phpimg->setTextColor($text_color);

		$phpimg->text(get_field('site_title', 'option'), array(
	        'fontSize' => 60, 
	        'x' => 0,
	        'y' => 0,
	        'width' => 560,
	        'height' => 128,
	        'alignHorizontal' => 'center',
	        'alignVertical' => 'center',
	    ));

		$phpimg->imagetrim();

		$phpimg->setOutput('png');

		$phpimg->save(wp_upload_dir()['basedir'] . '/logo-text.png');
		chmod(wp_upload_dir()['basedir'] . '/logo-text.png', 0755);
	}

	// Render Title for Footer 'Links' Section
	function action_123_before_mobile_footer_social_links(){
		?>
			<h2 class="fade mobilefooter-h2">extra links</h2>
		<?php
	}
	add_action( '123_before_mobile_footer_social_links', 'action_123_before_mobile_footer_social_links' );

	add_action( '123_before_desktop_footer_social_links', 'action_123_before_desktop_footer_social_links' );

	function action_123_before_desktop_footer_social_links(){
		?>
		<h2 class="fade footer-h2">extra Links</h2>
		<?php
	}

	function render_page_hero($classname){
		// error_log(get_field($classname . '-header', 'option'));
		$headertext = get_field($classname . '-alt-toggle', 'option') ? get_field($classname . '-alt', 'option') : $classname;
		$bgfieldname = '';
		switch ($classname) {
			case 'blog':
				$bgfieldname = 'general-blog-bg';
				break;
			
			case 'coupons':
				$bgfieldname = 'general-coupons-bg';
				break;

			default:
				$bgfieldname = $classname . '-bg';
				break;
		}
		$bgimagestring = '';
		if( $classname !== 'areas-served' ){
			$bgimagestring = 'style="background-image: url(' . get_field($bgfieldname, 'option') . ');"';
		}
		?>	
			<section class="<?php echo $classname ?>-hero hero"<?php echo $bgimagestring; ?>>
				<div class="<?php echo $classname ?>-hero-text hero-text">
					<h1 class="<?php echo $classname ?>-hero-text-header hero-text-header"><?php echo str_replace('-', ' ', $headertext); ?></h1>
					<?php echo do_action( '123_action_after_page_hero_header' ); ?>
				</div>
				<div class="<?php echo $classname ?>-hero-tint hero-tint"></div>
				<?php if( $classname == 'areas-served' ): ?>
					<div class="areas-served-hero-map" id="map"></div>
				<?php endif; ?>
				<?php if( $classname == 'contact' ): ?>
					<div class="contact-hero-map"></div>
				<?php endif; ?>
			</section>
			<section class="herocontent">
				<div class="<?php echo $classname ?>-herocontent-text herocontent-text">
					<h1 class="<?php echo $classname ?>-herocontent-text-header herocontent-text-header"><?php echo str_replace('-', ' ', $headertext); ?></h1>
					<?php echo do_action( '123_action_after_page_hero_header' ); ?>
				</div>
			</section>
		<?php
	}

	/*
		Color Pickers

	*/
		function action_123_after_color_pickers() {
			// Nav Background
			if (get_field('navs-bg-toggle', 'option') ) : 
				$color = get_field('navs-bg', 'option');
				?>
				<style type="text/css">
				</style>
				<?php
			endif;
			// Nav Logo and Links
			if (get_field('navs-text-toggle', 'option') ) : 
				$color = get_field('navs-text', 'option');
				?>
				<style type="text/css">
					/* New Additions*/
					.header-content-logo-image {
						filter: none;
					}
				</style>
				<?php
			endif;
			// Accent Color
			if (get_field('buttons-underlines-toggle', 'option') ) : 
				$color = get_field('buttons-underlines', 'option');
				?>
				<style type="text/css">
					/* Accent as the Hover for Buttons */
					.footer-sociallinks-item:hover,
					.sociallink:hover,
					.blog-blog-grid-item-socialcontainer-link.sociallink:hover,
					.company-employees-grid-item-socialcontainer-link:hover {
						background-color: <?php echo $color; ?>;
					}
					/* read more (hover was pink)*/
					.global-recentposts-grid-item-link:hover {
						color: <?php echo $color; ?>;
					}
					/* ... */
					.superheader,
					.home-quickquote-wrapper-right-button,
					.gform_footer input[type='submit'],
					.mobileheaderquote {
						background-color: <?php echo $color; ?>;
					}
					.section-header:after,
					.global-recentposts-grid-item-header:before,
					.services-services-grid-item-header:after,
					.contact-contact-left-locations-header:after,
					.blog-blog-grid-item-textcontainer-header:after,
					.blog-blog-sidebar-archive-header:after,
					.blog-blog-sidebar-categories-header:after,
					.blog-blog-sidebar-recentposts-header:after,
					.gallery-galleries-gallery-header:after {
						border-bottom: 6px solid <?php echo $color; ?>;
					}
					.home-testimonials-grid-item-quotemark,
					.global-recentposts-grid-item-link:after,
					.global-recentposts-viewall:after,
					.global-contact-content-button:after,
					.testimonials-testimonials-grid-item-quotemark,
					/*'angle-right' w/ default accent color */
					.home-areasserved-map-learnmore i,
					div.superheader-grid-item-button i,
					.home-areasserved-map-learnmore i,
					.home-services-viewall i,
					.home-testimonials-viewall i,
					.home-cover-content-button i,
					.mobileheaderquote-wrapper-button i {
						color: <?php echo $color; ?>;
					}
				</style>
				<?php
			endif;
			// Section Headers
			if (get_field('bold-title-text-toggle', 'option') ) : 
				$color = get_field('bold-title-text', 'option');
				?>
				<style type="text/css">
					
					/* From Parent */
					.section-header{
						color: <?php echo $color ?>;
					}
					<?php if( wp_get_theme()->get('Name') == '123 - Parent' ): ?>
						section:not(.home-hero):not(#coupons):not(#blog) .hero-text-header{
							color: <?php echo $color ?>;	
						}
					<?php endif; ?>
				</style>
				<?php
			endif;
			// Coupon / Sidebar / Areas Served BG
			if (get_field('sidebar-coupon-areasserved-bg-toggle', 'option') ) : 
				$color = get_field('sidebar-coupon-areasserved-bg', 'option');
				?>
				<style type="text/css">
				</style>
				<?php
			endif;
			// Button Backgrounds
			if (get_field('add_extra_theme_colors_button-bgtoggle', 'option') ) : 
				$color = get_field('add_extra_theme_colors_button-bgpicker', 'option');
				?>
				<style type="text/css">
					/* New Field */
					.home-areasserved-map-learnmore,
					.mobileheaderquote-wrapper-button,
					.global-contact-content-button,
					.gform_footer input[type='submit'],
					 a.global-recentposts-viewall,
					 div.superheader-grid-item-button,
					 a.blog-blog-sidebar-quickquote,
					.home-cover-content-button,
					.footer-sociallinks-item,
					.blog-blog-grid-item-socialcontainer-link.sociallink,
					.mobilefooter-sociallinks-item,
					.company-employees-grid-item-socialcontainer-link {
						background-color: <?php echo $color; ?>;
					}
					.superheader-grid-item-icon,
					.mobileheaderinfo-grid-item-left,
					.mobileheader-menus-social-menu-item-link i,
					.mobileheader-bar-menutoggle-icon {
						color: <?php echo $color; ?>;
					}
					/* Hack Harder...*/
					.home-services-viewall,
					.home-testimonials-viewall {
						background-color: <?php echo $color; ?> !important;
					}


				</style>
				<?php
			endif;
			// Button Text
			if (get_field('add_extra_theme_colors_button-texttoggle', 'option') ) : 
				$color = get_field('add_extra_theme_colors_button-textpicker', 'option');
				?>
				<style type="text/css">
					/* All the 'Button Background' elements should have 'Button Color' here*/
					.superheader-grid-item-content,
					.home-areasserved-map-learnmore,
					.home-quickquote-wrapper-right-button,
					.mobileheaderquote-wrapper-button,
					.home-services-viewall,
					.home-testimonials-viewall,
					.global-contact-content-button,
					.gform_footer input[type='submit'],
					 a.global-recentposts-viewall,
					 div.superheader-grid-item-button,
					 a.blog-blog-sidebar-quickquote,
					.home-cover-content-button.estimate-toggle,
					.home-areasserved-map-learnmore,
					.home-areasserved-map-learnmore i {
						color: <?php echo $color; ?>;
					}
					/* FontAwesome Icons*/
					.footer-sociallinks-item-link-icon,
					.company-employees-grid-item-socialcontainer-link-icon,
					.footer-sociallinks-item-link-icon:before,
					.mobilefooter-sociallinks-item-link-icon,
					.mobilefooter-sociallinks-item-link-icon:before,
					.sociallink-icon {
						color: <?php echo $color; ?>;
					}
				</style>
				<?php
			endif;
			// Footer Background
			if (get_field('add_extra_theme_colors_footer-bgtoggle', 'option') ) : 
				$color = get_field('add_extra_theme_colors_footer-bgpicker', 'option');
				?>
				<style type="text/css">
					/*remove the color-inverting filter from the logo when a color is set*/
					.footer,
					.mobilefooter,
					.footer-copyright {
						background-color: <?php echo $color; ?>;
					}
				</style>
				<?php
			endif;
			// footer headers and their underlines
			if (get_field('add_extra_theme_colors_footer-headerstoggle', 'option') ) : 
				$color = get_field('add_extra_theme_colors_footer-headerspicker', 'option');
				?>
				<style type="text/css">
					h2.footer-h2 {
						color: <?php echo $color; ?>;
					}
					h2.footer-h2:after{
						border-bottom: 1px solid <?php echo $color; ?>;
					}
					h2.mobilefooter-h2 {
						color: <?php echo $color; ?>;
					}
					h2.mobilefooter-h2:after{
						border-bottom: 1px solid <?php echo $color; ?>;
					}
				</style>
				<?php
			endif;
			// Footer Text
			if (get_field('add_extra_theme_colors_footer-texttoggle', 'option') ) : 
				$color = get_field('add_extra_theme_colors_footer-textpicker', 'option');
				?>
				<style type="text/css">
					/* New Field */

					.mobilefooter-logo-text,
					.mobilefooter-copyright p,
					.mobilefooter-contactlinks-phone,
					.mobilefooter-contactlinks-address,
					.mobilefooter-copyright-tclink,
					.mobilefooter-copyright a,
					.mobilefooter-pagelinks-item-link,
					/*desktop*/
					.footer-copyright,
					.footer-copyright a,
					.footer-contactlinks-address,
					.footer-contactlinks-phone,
					.footer-pagelinks-item-link,
					.mobilefooter-contactlinks-phone,
					.mobilefooter-copyright-tclink,
					.footer-pagelinks-item-link,
					.footer-contactlinks-phone {
						color: <?php echo $color; ?>;
					}
					/* Target new ChildTheme elements*/
					.mobile-footer-logo-text,
					.footer-logo-text {
						color: <?php echo $color; ?>;
					}
				</style>
				<?php
			endif;

		}
		add_action('123_after_color_pickers', 'action_123_after_color_pickers');


		// Setup add new Child Theme ColorPickers
		function add_colorpickers(){
			// Set the 'hidden' Parent Group (no label)
			$parent = 'field_123_colorpicker_wrapper';
			// List new Fields to Make : acf 'label' => acf 'name'
			$choices = [
				'Nav Logo Text'								=> 'header-logo',
				'Button Background'							=> 'button-bg',
				'Button Text'								=> 'button-text',
				'Footer Background'							=> 'footer-bg',
				'Footer Headers + Underlines'		 		=> 'footer-headers',
				'Footer Text'								=> 'footer-text',
			];
			// Create new Fields
			foreach ($choices as $label => $name) {
				$key = 'field_cp_' . $name;
				acf_add_local_field(array(
					'key' => $key . 'picker',
					'label' => $label,
					'name' => $name . 'picker',
					'type' => 'color_picker',
					'parent' => $parent,
					'wrapper' => array(
						'width' => 25,
					),
				));
				acf_add_local_field(array(
					'key' => $key . 'toggle',
					'label' => 'On / Off',
					'name' => $name . 'toggle',
					'type' => 'true_false',
					'ui' => 1,
					'parent' => $parent,
					'wrapper' => array(
						'width' => 75,
					),
				));
			}
		}
		// Initialize add new Child Theme ColorPickers
		add_action( 'acf/init', 'add_colorpickers', 11 );


		
		function adjust_colorpicker_values($field){
			$cps_to_be_hidden = array(
				'field_29wfeauajhadfsk',		// top-bottom-bg
				'field_faauoiegwuf23',			// top-bottom-bg-toggle
			);
			// Change Parent Labels:
			$cp_parents_to_rename = array(
				'field_8123afaasfdaef', 			// navs-bg
				'field_21387fzadsf',				// navs-text
				'field_27fzzzzadsf',				// buttons-underlines
				// 'field_2zcvvvczsf',				// bold-title-text
				// 'field_2123asaf',				// sidebar-coupon-areasserved-bg
			);
			// Give Parent Group a CSS Class
			$parent = 'field_123_colorpicker_wrapper';

			if( $field['key'] == $parent){
				$field['wrapper'] = array(
					'width' => '',
					'class' => '',
					'id' => 'colorpicker_hiddengroup',
				);

			}
			if( in_array($field['key'], $cp_parents_to_rename) ){
				$field['label'] = ' ';
				
				if($field['key'] == 'field_8123afaasfdaef' ){
					$field['label'] = 'Nav Background';
				}
				if($field['key'] == 'field_21387fzadsf' ){
					$field['label'] = 'Nav Links';
				}
				if($field['key'] == 'field_27fzzzzadsf' ){
					$field['label'] = 'Accent Color';
				}
			}
			if( in_array( $field['key'], $cps_to_be_hidden) ){
				unset($field);
			}
			else {
				return $field;
			}
		}

		add_filter('acf/load_field', 'adjust_colorpicker_values', 12);


		// Finally, parent group to appear seamlessly with the parent fields
		function my_acf_admin_head() {
			$screen = get_current_screen();
			if ( $screen->id = "toplevel_page_general-settings" ) :
				?>
					<style type="text/css">
						
						#colorpicker_hiddengroup {
							padding: 0;
							border: none;
							border-top: 1px solid #eeeeee;
						}
						#colorpicker_hiddengroup .acf-fields.-border {
							border: none;
						}

						#colorpicker_hiddengroup > .acf-label{
							display: none;
						}
					</style>
				<?php
			endif;
		}
		add_action('acf/input/admin_head', 'my_acf_admin_head');

	/*
		End Color Pickers
	*/

?>