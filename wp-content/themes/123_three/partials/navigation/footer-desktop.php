<footer class="footer<?php echo get_field('general-theme-invert-headerfooter-logo-colors', 'option') ? ' invertlogo' : ''; ?>">
	<div class="fade fade-in footer-wrap">

		

		<section class="footer-column footer-leftcolumn">
			
			<a href="<?php echo site_url(); ?>" class="footer-logo">
				<?php if( get_field('logo-type-switch', 'option') == 'text' ): ?>
					<h1 class="footer-logo-text"><?php echo get_field('site_title', 'option'); ?></h1>
				<?php else: ?>
					<img src="<?php echo get_logo(); ?>" class="footer-logo-image">
				<?php endif; ?>
			</a>
			<div class="footer-contactlinks">
				<?php if( !empty(get_the_phone()) ): ?>
					<a href="tel:<?php echo get_the_phone('tel'); ?>" class="footer-contactlinks-phone"><?php echo 'P: ' . get_the_phone() ?></a>
				<?php endif; ?>
				<?php if( !empty(get_the_fax()) ): ?>
					<a href="tel:<?php echo get_the_fax('tel'); ?>" class="footer-contactlinks-fax"><?php echo 'F: ' . get_the_fax() ?></a>
				<?php endif; ?>
				<?php if( !empty(get_the_address()) ) :
					$plainaddress = get_the_address();
					$breakat = strpos($plainaddress, ',');
					$insert = '<br>';
					$prettyaddress = substr_replace($plainaddress, $insert, $breakat, 1);
				 ?>
					<div class="footer-contactlinks-address"><?php echo $prettyaddress; ?></div>
				<?php endif; ?>
			</div>

			<?php  
				$facebook_link = get_field('social-facebook-link', 'option');
				$twitter_link = get_field('social-twitter-link', 'option');
				$instagram_link = get_field('social-instagram-link', 'option');
				$youtube_link = get_field('social-youtube-link', 'option');
				$googleplus_link = get_field('social-googleplus-link', 'option');
				$yelp_link = get_field('social-yelp-link', 'option');
				if( !empty($facebook_link) ||  !empty($twitter_link) || !empty($instagram_link) || 
					!empty($youtube_link) || !empty($googleplus_link)|| !empty($yelp_link)) :
			?>
			<ul class="footer-sociallinks">
				<?php if( !empty($facebook_link) ): ?>
				<li class="footer-sociallinks-item">
					<a href="<?php echo $facebook_link ?>" target="_blank" class="footer-sociallinks-item-link">
						<i class="footer-sociallinks-item-link-icon fa fa-facebook"></i>
					</a>
				</li>
				<?php endif; ?>
				<?php if( !empty($twitter_link) ): ?>
				<li class="footer-sociallinks-item">
					<a href="<?php echo $twitter_link ?>" target="_blank" class="footer-sociallinks-item-link">
						<i class="footer-sociallinks-item-link-icon fa fa-twitter"></i>
					</a>
				</li>
				<?php endif; ?>
				<?php if( !empty($instagram_link) ): ?>
				<li class="footer-sociallinks-item">
					<a href="<?php echo $instagram_link ?>" target="_blank" class="footer-sociallinks-item-link">
						<i class="footer-sociallinks-item-link-icon fa fa-instagram"></i>
					</a>
				</li>
				<?php endif; ?>
				<?php if( !empty($youtube_link) ): ?>
				<li class="footer-sociallinks-item">
					<a href="<?php echo $youtube_link ?>" target="_blank" class="footer-sociallinks-item-link">
						<i class="footer-sociallinks-item-link-icon fa fa-youtube-play"></i>
					</a>
				</li>
				<?php endif; ?>
				<?php if( !empty($googleplus_link) ): ?>
				<li class="footer-sociallinks-item">
					<a href="<?php echo $googleplus_link ?>" target="_blank" class="footer-sociallinks-item-link">
						<i class="footer-sociallinks-item-link-icon fa fa-google-plus"></i>
					</a>
				</li>
				<?php endif; ?>
				<?php if( !empty($yelp_link) ): ?>
				<li class="footer-sociallinks-item">
					<a href="<?php echo $yelp_link ?>" target="_blank" class="footer-sociallinks-item-link">
						<i class="footer-sociallinks-item-link-icon fa fa-yelp"></i>
					</a>
				</li>
				<?php endif; ?>
			</ul>
			<?php endif; ?>
		</section>
		
		
		<section class="footer-column footer-middlecolumn1">
			<?php do_action('123_before_desktop_footer_social_links'); ?>
			<?php render_page_links('footer-pagelinks', 'footer-pagelinks-item', 'footer-pagelinks-item-link'); ?>
		</section>
		

		<?php if( !empty(get_field('mastercard', 'option')) || !empty(get_field('visa', 'option')) || !empty(get_field('amex', 'option')) || !empty(get_field('discover', 'option')) || !empty(get_field('paypal', 'option')) ): ?>
			<section class="footer-column footer-middlecolumn2">
				<h2 class="footer-middlecolumn2-payment footer-h2">payment</h2>
				<div class="footer-middlecolumn2-payment--wrapper">
					<?php 
						$payment_types = array('mastercard', 'visa', 'amex', 'discover', 'paypal', 'cash', 'check');
						$payment_type_image_names = array('mc.png', 'visa.png', 'amex.png', 'discover.png', 'pp.png', 'cash.png', 'check.png');
						foreach($payment_types as $index => $payment_type){
							if( get_field($payment_type, 'option') == true ){
								$image_url = !empty(get_field($payment_type . '-image', 'option')) ? get_field($payment_type . '-image', 'option') : get_template_directory_uri() . '/library/img/payment_types/' . $payment_type_image_names[$index];
								?>
								<img src="<?php echo $image_url; ?>" class="footer-midlecolumn2-payment-type-image">
					<?php
							}
						}
					?>
				</div>
			</section>
		<?php endif; ?>
		
		<section class="footer-column footer-rightcolumn">
			<!-- webx logo -->
			<h2 class="footer-h2">Powered by: </h2>
			<a href="<?php echo !empty( get_field( 'webx-url', 'option' ) ) ? get_field( 'webx-url', 'option' ) : 'http://webxmarketing.com'; ?>" class="footer-webxlink">
				<img src="<?php echo get_field('webx-logo', 'option'); ?>" class="footer-webxlink-logo">
			</a>
		</section>

	</div>


</footer>
<div class="footer-copyright">
	Powered by <a target="_blank" class="footer-copyright-tclink" href="<?php the_field('webx-url', 'option') ?>"><?php the_field('webx-name', 'option'); ?></a> 
	<?php if( !get_field('terms-disable', 'option') ): ?>
		| <a class="footer-copyright-tclink" href="<?php echo site_url() ?>/terms">Terms &amp; Conditions</a> 
	<?php endif; ?>
	| Copyright &copy; <?php echo Date('Y') ?>
</div>