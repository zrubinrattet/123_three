;(function ( $, Theme, window, document, undefined ) {

	$(document).ready(function(){

		Theme.EmployeeGrid = {
			elem : document.querySelector('.company-employees-grid'),
			_init : function(){
				if( Theme.EmployeeGrid.elem !== null )
				$(window).on('load', Theme.EmployeeGrid._createMasonryGrid);
			},
			_createMasonryGrid : function(){
				Theme.EmployeeGrid.masonryGrid = new Masonry(Theme.EmployeeGrid.elem, {
					itemSelector : '.company-employees-grid-item',
				});
			}
		};

		Theme.EmployeeGrid._init();

		Theme.ServicesGrid = {
			elem : document.querySelector('.services-services-grid'),
			_init : function(){
				if( Theme.ServicesGrid.elem !== null ){
					$(window).on('load', Theme.ServicesGrid._createMasonryGrid);
				}
			},
			_createMasonryGrid : function(){
				Theme.EmployeeGrid.masonryGrid = new Masonry(Theme.ServicesGrid.elem, {
					itemSelector : '.services-services-grid-item',
				});	
			}
		};

		// Theme.ServicesGrid._init(); // kmedit

		Theme.BlogGrid = {
			elem : document.querySelector('.blog-blog-grid'),
			_init : function(){
				if( Theme.BlogGrid.elem !== null ){
					$(window).on('load', Theme.BlogGrid._createMasonryGrid);
				}
			},
			_createMasonryGrid : function(){
				Theme.EmployeeGrid.masonryGrid = new Masonry(Theme.BlogGrid.elem, {
					itemSelector : '.blog-blog-grid-item',
				});	
			}
		};

		Theme.BlogGrid._init();

		Theme.TestimonialsGrid = {
			elem : document.querySelector('.testimonials-testimonials-grid'),
			_init : function(){
				if( Theme.TestimonialsGrid.elem !== null ){
					$(window).on('load', Theme.TestimonialsGrid._createMasonryGrid);
				}
			},
			_createMasonryGrid : function(){
				Theme.EmployeeGrid.masonryGrid = new Masonry(Theme.TestimonialsGrid.elem, {
					itemSelector : '.testimonials-testimonials-grid-item',
				});	
			}
		};

		Theme.TestimonialsGrid._init();
	});

})( jQuery, Theme, window, document );

